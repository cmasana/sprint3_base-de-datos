
/*
 * CREACIÓN DE TABLAS EN SQL
 * DESCRIPCIÓN: SPRINT 3
 * AUTORES: DAW 2
 *
 * CAMBIOS:
 * #1# AÑADIDO CAMPO STATUS A TABLA USER Y PROPOSAL 
 * #2# MODIFICADA LONGITUD DE TODOS LOS VARCHAR
 * #3# CAMBIOS DATETIME A DATE EN ALGUNOS CAMPOS
 * #4# CAMBIOS VARCHAR A TEXT EN 2 CAMPOS (BIO Y PROFILE_PIC)
 * #5# AÑADIDO ON DELETE/UPDATE CASCADE A TODAS LAS FOREIGN KEYS
 * #6# AÑADIDO DEFAULT A CAMPOS TIPO ENUM (EXCEPTO TABLA VOTE)
 * #7# AÑADIDOS IF EXISTS PARA CREAR TABLAS 
 */
 
/* tabla city */
create table if exists CITY (
    id_city int primary key auto_increment,
    cityname varchar(50) not null,
    regionstate varchar(50) not null,
    postalcode int not null
);

/* tabla role */
create table if exists ROLE (
    id_role int primary key auto_increment,
    rolename varchar(50) not null
);


/* tabla user */
create table if exists USER (
    id_user int primary key auto_increment,
    firstname varchar(50) not null,
    lastname varchar(50) not null,
    username varchar(50) not null,
    passwd varchar(50) not null,
    profilepic text, -- tipo de dato text: no puede pertenecer a un index /// longitud máxima: 65,535 caracteres
    email varchar(100) not null,
    id_city int not null,
    bio text,
    id_role int not null,
    dni varchar(10) not null,
    birthdate date not null,
    creationdate datetime not null,
	status enum('active', 'inactive') default 'active',
    unique (dni),
    unique (email),
    unique (username),
    foreign key (id_city) references city (id_city)
	on delete cascade
	on update cascade,
    foreign key (id_role) references role (id_role)
	on delete cascade
	on update cascade
);


-- para borrar constraints:
/* alter table user
drop unique (dni)
drop primary key; */

-- para añadir constraints:
/* alter table user
add primary key (iduser); */

-- posibles índices
/* alter table user
drop index idx_username; */
create index idx_username
    on user (username);
    
create index idx_email
    on user (email);
    

/* tabla school */
create table if exists SCHOOL (
    id_school int primary key auto_increment,
    schoolname varchar(100) not null,
    schoolemail varchar(100) not null,
    address varchar(100) not null,
    id_city int not null,
    phone int not null,
    schooltype varchar(50) not null,
    foreign key (id_city) references city (id_city)
	on delete cascade
	on update cascade,
	unique (schoolemail)
);


/* tabla school_users */
create table if exists SCHOOL_USERS (
    id_schooluser int primary key auto_increment,
    id_user int not null,
    id_school int not null,
    foreign key (id_user) references user (id_user)
	on delete cascade
	on update cascade,
    foreign key (id_school) references school (id_school)
	on delete cascade
	on update cascade
);


/* tabla sgroup */
create table if exists SGROUP (
    id_group int primary key auto_increment,
    groupname varchar(50) not null,
    creationdate datetime not null
);


/* tabla user_groups */
create table if exists USER_GROUPS (
    id_usergroups int primary key auto_increment,
    id_group int not null,
    id_user int not null,
    uniondate datetime not null,
    foreign key (id_group) references sgroup (id_group)
	on delete cascade
	on update cascade,
    foreign key (id_user) references user (id_user)
	on delete cascade
	on update cascade	
);


/* tabla proposal */
create table if exists PROPOSAL (
    id_proposal int primary key auto_increment,
    proposal_name varchar(100) not null,
    publication_date date not null,
    specs varchar(100) not null,
    description varchar(100) not null,
    professional_family varchar(50) not null,
    limit_date date not null,
	status enum('active','inactive','deleted') not null
);


-- posibles índices
create index idx_proposal_name
    on proposal (proposal_name);

create index idx_pub_date
    on proposal (publication_date);

create index idx_limit_date
    on proposal (limit_date);

create index idx_family
    on proposal (professional_family);


/* tabla user_proposals */
create table if exists USER_PROPOSALS (
    id_userprop int primary key auto_increment,
    id_user int not null,
    id_proposal int not null,
    publicationdate datetime not null,
    foreign key (id_user) references user (id_user)
	on delete cascade
	on update cascade,
    foreign key (id_proposal) references proposal (id_proposal)
	on delete cascade
	on update cascade
);


/* tabla school_proposals */
create table if exists SCHOOL_PROPOSALS (
    id_schoolprop int primary key auto_increment,
    id_school int not null,
    id_proposal int not null,
    foreign key (id_school) references school (id_school)
	on delete cascade
	on update cascade,
    foreign key (id_proposal) references proposal (id_proposal)
	on delete cascade
	on update cascade
);


/*tabla company*/
create table if exists COMPANY (
	id_company int primary key auto_increment,
	email varchar(100) not null,
	name varchar(50) not null,
	nif varchar(10) not null,
	address varchar(100) not null,
	city varchar(50) not null,
	postal_code int not null,
	phone_number varchar(50) not null,
	sector varchar(50) not null,
	id_city int not null,
	status enum('active','inactive') not null,
	unique(id_company),
	unique(nif),
	unique(email),
	foreign key (id_city) references city (id_city)
	on delete cascade
	on update cascade
);


/* tabla company_user */
create table if exists COMPANY_USERS (
	id_company_user int primary key auto_increment,
	id_user int not null,
	id_company int not null,
	foreign key (id_user) references user (id_user)
	on delete cascade
	on update cascade,
	foreign key (id_company) references company (id_company)
	on delete cascade
	on update cascade
);


/* taula tag */
create table if exists TAG (
	id_tag int primary key auto_increment,
	tagname varchar(50) not null
);


/* taula proposal_tag */
create table if exists TAGS (
	id_proposaltag int primary key auto_increment,
	id_proposal int not null,
	id_tag int not null,
	foreign key (id_proposal) references proposal (id_proposal)
	on delete cascade
	on update cascade,
	foreign key (id_tag) references tag (id_tag)
	on delete cascade
	on update cascade
);


/* taula project*/
create table if exists PROJECT (
    id_project int primary key auto_increment,
    project_name varchar(50) not null,
    initial_date date not null,
    ending_date date not null,
    budget int not null,
	description varchar(100) not null,
    professional_family varchar(50) not null,
	status enum('active', 'inactive') not null,
    foreign key (id_project) references proposal (id_proposal)
	on delete cascade
	on update cascade
);  

/*taula document_manager*/
create table if exists DOCUMENT_MANAGER (
	id_manager int primary key auto_increment,
	size int not null,
	dm_owner int not null,
	id_project int not null,
	foreign key (dm_owner) references user (id_user)
	on delete cascade
	on update cascade,
	foreign key (id_project) references project (id_project)
	on delete cascade
	on update cascade
);


/* taula dm_folder */
create table if exists DM_FOLDER (
    id_folder int primary key auto_increment,
    folder_name varchar (50) not null,
    creation_date date not null,
    id_document_manager int not null,
    foreign key (id_document_manager) references document_manager (id_manager)
	on delete cascade
	on update cascade
);


/* taula dm_file */
create table if exists DM_FILE (
	id_file int primary key auto_increment,
	name varchar(100) not null,
	last_modification date not null,
	size int not null,
	file_type varchar(20) not null,
	id_folder int not null,
	foreign key (id_folder) references dm_folder (id_folder)
	on delete cascade
	on update cascade
); 


/*taula dm_container_folder*/
create table if exists DM_CONTAINER_FOLDER (
    id_container_folder int primary key auto_increment,
    id_folder int not null,
    id_parent_folder int
);


-- blog
create table if exists BLOG (
    id_project int primary key auto_increment,
    title varchar(100) not null,
	foreign key (id_project) references project (id_project)
	on delete cascade
	on update cascade
);


-- post
create table if exists POST (
    id_post int primary key auto_increment,
    id_project int not null,
	title varchar(50) not null,
	content longtext not null,
	creation_date date not null,
	last_modified date,
	id_user int not null,
	status enum('active','inactive') not null,
	foreign key (id_user) references user (id_user)
	on delete cascade
	on update cascade,
	foreign key (id_project) references blog (id_project)
	on delete cascade
	on update cascade
);

-- comment
create table if exists COMMENT (
    id_comment  int primary key auto_increment,
    content  longtext not null,
    id_user  int not null,
    id_post  int not null,
    id_project int not null,
	foreign key (id_user) references user(id_user)
	on delete cascade
	on update cascade,
	foreign key (id_post) references post(id_post)
	on delete cascade
	on update cascade,
	foreign key (id_project) references post(id_project)
	on delete cascade
	on update cascade
);


-- vote
create table if exists VOTE (
    id_post int not null,
    id_project int not null,
	id_user int not null,
	value_vote enum('1', '-1') not null,
	primary key (id_post, id_project, id_user)
);

-- wiki
create table if exists WIKI (
    id_project int primary key auto_increment,
    title varchar(100) not null,
    foreign key (id_project) references project(id_project)
	on delete cascade
	on update cascade
);

-- article
create table if exists ARTICLE (
    id_article int primary key auto_increment,
    id_project int not null,
    title varchar(50) not null,
    content longtext not null,
    creation_date date not null,
    last_modified date,
	reference longtext,
	id_user int,
	status enum('active','inactive') not null,
	foreign key (id_project) references wiki (id_project)
	on delete cascade
	on update cascade,
	foreign key (id_user) references user (id_user)
	on delete cascade
	on update cascade
);

-- chat
create table if exists CHAT (
    id_chat int primary key auto_increment,
    owner int not null,
	chat_name varchar (30) not null,
	description text
);

-- chat_member
create table if exists CHAT_MEMBER (
    id_chat int not null,
    id_user int not null,
	foreign key (id_chat) references chat (id_chat)
	on delete cascade
	on update cascade,
	foreign key (id_user) references user (id_user)
	on delete cascade
	on update cascade
);

-- añadir fk a tabla chat
alter table chat
add constraint 
foreign key (owner) references chat_member (id_user) on delete cascade on update cascade;


-- message
create table if exists MESSAGE (
    id_chat int not null,
    id_user int not null,
    content longtext not null,
    datetime datetime not null,
    primary key (id_user, datetime),
    foreign key (id_chat) references chat (id_chat)
	on delete cascade
	on update cascade,
    foreign key (id_user) references user (id_user)
	on delete cascade
	on update cascade
);

-- mail_message
create table if exists MAIL_MESSAGE (
    id_message int primary key auto_increment,
    id_user int not null,
    reciever int not null,
    subject varchar(200) not null,
    content longtext not null,
    datetime datetime not null,
	delete_sender enum('no','yes') not null,
	delete_receiver enum('no','yes') not null,
    foreign key (id_user) references user (id_user)
	on delete cascade
	on update cascade,
    foreign key (reciever) references user (id_user)
	on delete cascade
	on update cascade
);

-----------------

-- resource center
create table if exists RESOURCE_CENTER (
	id_rc int primary key auto_increment,
	size int not null,
	rc_manager int not null,
	foreign key (rc_manager) references user (id_user)
	on delete cascade
	on update cascade
);

-- rc folder
create table if exists RC_FOLDER (
	id_folder int primary key auto_increment,
	name varchar(50) not null,
	creation_date datetime not null,
	id_rc int not null,
	foreign key (id_rc) references resource_center (id_rc)
	on delete cascade
	on update cascade
);

-- rc container folder
create table if exists RC_CONTAINER_FOLDER (
	id_container_folder int primary key auto_increment,
	id_folder int not null,
	id_parent_folder int not null,
	foreign key (id_folder) references rc_folder (id_folder)
	on delete cascade
	on update cascade
);

-- rc file
create table if exists RC_FILE (
	id_file int primary key auto_increment,
	id_folder int not null,
	name varchar(50) not null,
	last_modification datetime not null,
	size int not null,
	file_type varchar(20) not null,
	foreign key (id_folder) references rc_folder (id_folder)
	on delete cascade
	on update cascade
);

-- ticket
create table if exists TICKET (
  id_incidence int primary key auto_increment,
  topic varchar(50) not null,
  type varchar(50) not null,
  priority varchar(50) not null,
  creation_date datetime not null,
  solved_date datetime not null,
  id_assigned_user int not null,
  id_author int not null,
  status enum('pending','in progress','resolved') not null,
  foreign key (id_assigned_user) references user (id_user)
  on delete cascade
  on update cascade,
  foreign key (id_author) references user (id_user)
  on delete cascade
  on update cascade
);




